<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $table = 'chat';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    protected $guarded = [];

    public function pelapor()
    {
        return $this->belongsTo(Pelapor::class, 'id_pelapor', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user', 'id');
    }

    public function chatMessage()
    {
        return $this->hasMany(ChatMessage::class, 'id_chat', 'id');
    }
}

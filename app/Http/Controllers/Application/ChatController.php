<?php

namespace App\Http\Controllers\Application;

use App\Events\SendChat;
use App\Http\Controllers\Controller;
use App\Models\Chat;
use App\Models\ChatMessage;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        $chat = Chat::with('pelapor', 'user')->where('id_user', auth()->id())->orWhere('id_user', null)->orderBy('session_start', 'desc')->get();

        if ($request->ajax()) {
            $view = view('app.chat.list', compact('chat'))->render();
            return response()->json(['html' => $view]);
        }

        return view('app.chat.index', compact('chat'));
    }

    public function show(Request $request, $id_chat)
    {
        $chat = Chat::with(['pelapor', 'user'])->where('id', $id_chat)->first();
        $chat_message = ChatMessage::where('id_chat', $id_chat)->get();
        return response()->json(['status' => 200, 'chat' => $chat, 'chat_message' => $chat_message]);
    }

    public function getMessage($id)
    {
        $chat_message = ChatMessage::where('id', $id)->first();
        if ($chat_message->count() > 0) {
            return response()->json(['status' => 200, 'chat_message' => $chat_message]);
        }

        return response()->json(['status' => 400, 'message' => $chat_message]);
    }

    public function send(Request $request)
    {
        if ($request->body) {
            Chat::where('id', $request->id_chat)->update([
                'id_user' => auth()->id(),
            ]);
            ChatMessage::where('id_chat', $request->id_chat)->where('is_pelapor', true)->update([
                'last_seen' => now(),
            ]);
            $chat_message = ChatMessage::create([
                'id_chat' => $request->id_chat,
                'is_pelapor' => false,
                'body' => $request->body,
                'last_seen' => null,
            ]);

            event(new SendChat($chat_message));

            return response()->json(['status' => 200, 'chat_message' => $chat_message]);
        } else {
            return response()->json(['status' => 400]);
        }
    }
}

$(document).ready(function () {
    function loadChatList() {
        $.ajax({
            url: window.location.href,
            type: "GET",
        }).done(function (response) {
            if (response.html == " ") {
                return;
            }
            $(".chat_list").empty();
            $(".chat_list").append(response.html);
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            alert("Server Not Responding...");
        });
    }

    // Show Detail Message
    $('body').on('click', '.btn_detail_chat', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            dataType: 'JSON',
            beforeSend: function () {
                $(".no_chat_message").addClass('d-none');
            },
            success: function (response) {
                $('.message_from').html(response.chat.pelapor.nama_lengkap);
                $('.id_chat').val(response.chat.id);
                $.each(response.chat_message, function (index, data) {
                    var created_at = new Date(data.created_at);
                    var jam = created_at.getHours();
                    var min = created_at.getMinutes();
                    if (min < 10)
                        minute = "0" + min;
                    else
                        minute = min;
                    html = '';
                    if (data.is_pelapor == 1) {
                        html += `<div class="d-flex justify-content-start mb-5 chat_message">
                                    <div class="d-flex flex-column align-items-start">
                                        <div class="py-3 px-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start">
                                            ` + data.body + `
                                            <br><small class="text-muted">`+ jam + ":" + minute + `</small>
                                        </div>
                                    </div>
                                </div>`
                            ;
                    } else {
                        html += `<div class="d-flex justify-content-end mb-5 chat_message">
                                    <div class="d-flex flex-column align-items-end">
                                        <div class="py-3 px-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end">
                                            ` + data.body + `
                                            <br><small class="text-muted">`+ jam + ":" + minute + `</small>
                                        </div>
                                    </div>
                                </div>`
                            ;
                    }
                    $('.chat_body').append(html);
                    $(".chat_body").scrollTop($(".chat_body")[0].scrollHeight);
                });
                $(".chat_message").removeClass("d-none");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                $(".chat_message").addClass('d-none');
                $(".no_chat_message").removeClass('d-none');
            }
        });
    });

    // Sending Chat
    $(".btn_send_chat").on("click", function (event) {
        event.preventDefault();
        var form = $('.form_chat'),
            url = form.attr('action'),
            method = 'POST';
        $.ajax({
            url: url,
            method: method,
            data: form.serialize(),
            success: function (response) {
                if (response.status == 200) {
                    var created_at = new Date(response.chat_message.created_at);
                    var jam = created_at.getHours();
                    var min = created_at.getMinutes();
                    if (min < 10)
                        minute = "0" + min;
                    else
                        minute = min;
                    html = '';
                    html += `<div class="d-flex justify-content-end mb-5 chat_message">
                                <div class="d-flex flex-column align-items-end">
                                    <div class="py-3 px-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end">
                                        ` + response.chat_message.body + `
                                        <br><small class="text-muted">`+ jam + ":" + minute + `</small>
                                    </div>
                                </div>
                            </div>`
                        ;
                    $('.chat_body').append(html);
                    $("#body").val("");
                    $(".chat_body").scrollTop($(".chat_body")[0].scrollHeight);
                } else {
                    console.log('404 Not Found');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                $(".chat_message").addClass('d-none');
                $(".no_chat_message").removeClass('d-none');
            }
        });
    });

    // Get Chat form pusher
    var pusher = new Pusher('78256e98206ec9b69fb1', {
        cluster: 'ap1'
    });
    var channel = pusher.subscribe('chat-channel');
    channel.bind('chat-event', function (response) {
        if (JSON.stringify(response.chat.id_chat) == $(".id_chat").val()) {
            $.ajax({
                url: window.location.href + '/get/' + JSON.stringify(response.chat.id),
                method: 'get',
                dataType: 'JSON',
                success: function (response) {
                    if (response.status == 200) {
                        var created_at = new Date(response.chat_message.created_at);
                        var jam = created_at.getHours();
                        var min = created_at.getMinutes();
                        if (min < 10)
                            minute = "0" + min;
                        else
                            minute = min;
                        html = '';
                        html += `<div class="d-flex justify-content-start mb-5 chat_message">
                                    <div class="d-flex flex-column align-items-start">
                                        <div class="py-3 px-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start">
                                            ` + response.chat_message.body + `
                                            <br><small class="text-muted">`+ jam + ":" + minute + `</small>
                                        </div>
                                    </div>
                                </div>`
                            ;
                        $('.chat_body').append(html);
                        $("#body").val("");
                        $(".chat_body").scrollTop($(".chat_body")[0].scrollHeight);
                    } else {
                        console.log(response.chat_message);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    $(".chat_message").addClass('d-none');
                    $(".no_chat_message").removeClass('d-none');
                }
            });
            $('audio#audio_notifikasi')[0].play();
        }
        loadChatList();
    });
});

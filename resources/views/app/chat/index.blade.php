@extends('layouts.app.template')
@section('content')
    <div class="toolbar py-5 py-lg-15" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <h1 class="d-flex text-white fw-bolder my-1 fs-3">Chat</h1>
                <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
                    <li class="breadcrumb-item text-white opacity-75">
                        <a href="{{ url('/') }}" class="text-white text-hover-primary">Beranda</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-white opacity-75 w-5px h-2px"></span>
                    </li>
                    <li class="breadcrumb-item text-white opacity-75">Chat</li>
                </ul>
            </div>
        </div>
    </div>
    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">
        <div class="content flex-row-fluid" id="kt_content">
            <div class="d-flex flex-column flex-lg-row">
                <div class="flex-column flex-lg-row-auto w-100 w-lg-300px w-xl-400px mb-10 mb-lg-0">
                    <div class="card card-flush">
                        <div class="card-header pt-5" id="kt_chat_contacts_header">
                            <h5 class="card-title">Chat Masuk</h5>
                        </div>
                        <div class="card-body pt-3" id="chat_list">
                            <div class="scroll-y me-n5 pe-5 h-500px h-lg-auto chat_list" data-kt-scroll="true"
                                data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto"
                                data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_contacts_header"
                                data-kt-scroll-wrappers="#kt_content, #kt_chat_contacts_body" data-kt-scroll-offset="0px">
                                @include('app.chat.list')
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
                    <div class="card chat_message d-none" id="chat_message">
                        <div class="card-header" id="chat_header">
                            <div class="card-title">
                                <div class="d-flex justify-content-center flex-column me-3">
                                    <span class="fs-4 fw-bolder text-gray-900 me-1 mb-2 lh-1 message_from"></span>
                                    <div class="mb-0 lh-1">
                                        <span class="badge badge-success badge-circle w-10px h-10px me-1"></span>
                                        <span class="fs-7 fw-bold text-muted">Pelapor</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="chat_body">
                            <div class="scroll-y me-n5 pe-5 h-300px h-lg-auto overflow-scroll chat_body"
                                data-kt-scroll="true" data-kt-scroll-max-height="auto"
                                data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer"
                                data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="-2px">
                            </div>
                        </div>
                        <div class="card-footer pt-4" id="kt_chat_messenger_footer">
                            <form class="form_chat" action="{{ url('chat/send') }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" class="id_chat" value="0" name="id_chat">
                                <div class="d-flex flex-stack">
                                    <div class="d-flex align-items-center me-2 w-100">
                                        <textarea name="body" id="body" class="form-control form-control-flush mb-3"
                                            rows="1" data-kt-element="input" placeholder="Kirim Pesan"></textarea>
                                    </div>
                                    <button class="btn btn-primary btn_send_chat btn-sm" type="submit">Kirim</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="card no_chat_message">
                        <div class="card-body p-0">
                            <div class="card-px text-center py-10 my-5">
                                <h2 class="fs-2x fw-bolder">Live Chat</h2>
                                <p class="text-gray-400 fs-4 fw-bold">Dukcapil Kota Cilegon.
                                    <br />Layanan chat untuk memudahkan pelapor dan petugas dalam berkomunikasi
                                </p>
                            </div>
                            <div class="text-center px-4">
                                <img class="mw-100 mh-200px" alt="logo"
                                    src="{{ asset('') }}assets/media/illustrations/sigma-1/2.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('') }}js/app/chat/index.js"></script>
@endpush
